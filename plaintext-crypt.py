#!/usr/bin/python3
from getpass import getpass
from hashlib import sha512
import sys
import argparse

def encode(string, pswhash, blocksize=128):
    string2 = ''
    for i in range(0,blocksize):
        if not i<len(string):
            break
        string2+=chr(ord(string[i])+ord(pswhash[i]))
    return string2

def decode(string, pswhash, blocksize=128):
    string2 = ''
    for i in range(0, blocksize):
        if not i<len(string):
            break
        string2+=chr(ord(string[i])-ord(pswhash[i]))
    return string2


def main():
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-e', '--encode', action='store_true', help='Encode mode')
    group.add_argument('-d', '--decode', action='store_true', help='Decode mode')
    parser.add_argument('-b', '--blocksize', default='128', help='e.g. 128 (default), 64, 30')

    parser.add_argument('filename', help='Specify the file to encode/decode')
    parser.add_argument('--version', action='version', version='%(prog)s 0.1')
    args = parser.parse_args()

    filename = args.filename
    blocksize=int(args.blocksize)

    psw = getpass('Password:')
    # pswhash = sha512(psw.encode('utf-8')).hexdigest()
    # if blocksize > len(pswhash):
    #     print("ERROR: blocksize bigger than the hash string.")
    #     sys.exit(0)

    # basically the intent is to add at the key a number that increase in every
    # cycle and calculate a different hash
    if args.encode:
        f = open(filename)
        f2 = open(filename+'.encoded','w')
        x = 0
        while True:
            string = f.read(blocksize)
            if not string:
                break
            pswhash = sha512((psw+str(x)).encode('utf-8')).hexdigest()
            f2.write(encode(string, pswhash))
            x+=1
        f.close
        f2.close
        print("File encoded.")

    try:
        if args.decode:
            f = open(filename+'.encoded')
            f2 = open(filename+'.decoded', 'w')
            x = 0
            while True:
                string = f.read(blocksize)
                if not string:
                    break
                pswhash = sha512((psw+str(x)).encode('utf-8')).hexdigest()
                f2.write(decode(string, pswhash))
                x+=1
            f.close
            f2.close
            print("File decoded.")
    except ValueError:
        print('Sorry, wrong password!')


if __name__ == "__main__":
    main()
