A modern version of the "Vigenere cipher".
The algorithm is well known for its weakness due to the repetition of the key.
This code is intended as a mere exercise. Its purpose is to create a weak
encrypted file to test decryption algorythms or tools.
I do not recommend in any way to protect sensitive information with this method.
This software is provided 'as-is' without any express or implied warranty.
I will no take any responsability for any data corruption or leak of sensitive 
information.
Take a look to the LICENSE file for further informations.

Author: Nicola Anzelmo
email: alocin05@gmail.com
